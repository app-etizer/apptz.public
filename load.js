/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import validateOptions from './validateOptions';

export function loadRemoteScreen(url, screenId, componentWrapper){
  return fetch(url)
  .then(res=> res.text())
  .then(source => {
    // Before executing eval() on the code, we want to make sure that when the screen is exported,
    // the variables 'module' and 'exports' both exist. After calling eval(), one of these should
    // contain our screen (depending on the manner of exporting).
    var exports = {}
    var module = {}

    // We'll inject 'react' so that it does not need to be included. Any other dependencies
    // need to ben rolled up within the screen bundle. In future, we can offer multiple platform
    // dependencies to reduce the screen bundle size.
    function require(name){
      if(name === 'react') return React
      else throw new Error(`You can't use modules other than "react" in remote component.`)
    }

    // Within a screen, components can be registered with the apptz platform using a
    // higher order function. There are three reasons to register a component:
    // - its static content is configurable, like a label or an icon
    // - it throws an event defined and described by the screen itself
    // - it shows data that is handled by the platform
    function register(options){
      // Validate the options passed to the function according to the options schema
      let validOptions = validateOptions(options);

      return (Comp) => {
        return componentWrapper ? componentWrapper(Comp, validOptions, screenId) : Comp;
      }
    }

    function mood(Comp){
      return connect(({mood}) => ({mood}))(Comp)
    }

    eval(source);

    return module.exports || (exports.__esModule ? exports.default : exports)
  }).catch(err => console.log('nope', err))
}
