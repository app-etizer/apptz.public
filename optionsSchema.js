export default {
  "definitions": {
    "field": {
      "type": "object",
      "required": ["name", "type"],
      "properties": {
        "name": {
          "type": "string"
        },
        "type": {
          "type": "string",
          "default": "text",
          "enum": ["text", "image", "icon", "price", "rating"]
        },
        "fields": {
          "type": "array",
          "items": { "$ref" : "#/definitions/field" }
        }
      }
    },
    "event": {
      "type": "object",
      "required": ["name"],
      "properties": {
        "name": {
          "type": "string"
        },
        "description": {
          "type": "string"
        }
      }
    }
  },

  "type": "object",
  "properties": {
    "events": {
      "type": "array",
      "items": { "$ref" : "#/definitions/event" }
    },
    "fields": {
      "type": "array",
      "items": { "$ref" : "#/definitions/field" }
    }
  }
}
