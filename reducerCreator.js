const mergeUnique = (firstArray, secondArray) => {
  let newArray = new Set();

  firstArray.map(i => newArray.add(i));
  secondArray.map(i => newArray.add(i));

  return [...newArray];
};

export const setActionType = name => `REDUCERCREATOR_${name}_SET`;
export const updateActionType = name => `REDUCERCREATOR_${name}_UPDATE`;

export const mergeActionType = name => `REDUCERCREATOR_${name}_MERGE`;
export const addActionType = name => `REDUCERCREATOR_${name}_ADD`;
export const addFirstActionType = name => `REDUCERCREATOR_${name}_ADD_FIRST`;
export const sortActionType = name => `REDUCERCREATOR_${name}_SORT`;

export const deleteActionType = name => `REDUCERCREATOR_${name}_DELETE`;
export const deleteLastActionType = name => `REDUCERCREATOR_${name}_DELETE_LAST`;
export const clearActionType = name => `REDUCERCREATOR_${name}_CLEAR`;

export const objectReducer = (name, initialValue = {}) => {
  let SET = setActionType(name);
  let UPDATE = updateActionType(name);
  let MERGE = mergeActionType(name);
  let DELETE = deleteActionType(name);
  let CLEAR = clearActionType(name);

  return (state = initialValue, a) => {
    switch(a.type) {
      case SET:
        return {...state, [a.id]: a.payload};
      case MERGE:
      case UPDATE:
        if(typeof a.payload === 'object'){
          if(Array.isArray(state[a.id]))
            return {...state, [a.id]: [...state[a.id], a.payload]};

          if(typeof state[a.id] === 'object')
            return {...state, [a.id]: {...state[a.id], ...a.payload}};

          return {...state, [a.id]: {...a.payload}};
        }

        if(Array.isArray(a.payload)){
          if(Array.isArray(state[a.id]))
            return {...state, [a.id]: mergeUnique(a.payload, state[a.id])};

          return {...state, [a.id]: [...a.payload]};
        }

        if(Array.isArray(state[a.id])){
          return {...state, [a.id]: mergeUnique(state[a.id], [a.payload])};
        }

        return {...state, [a.id]: a.payload};
      case DELETE:
        let { [a.payload]: value, ...remainder} = state;
        return remainder;
      case CLEAR:
        return {};
      default:
        return state;
    }
  };
}

export const arrayReducer = (name, initialValue = []) => {
  let SET = setActionType(name);
  let UPDATE = updateActionType(name);
  let MERGE = mergeActionType(name);
  let ADD = addActionType(name);
  let ADD_FIRST = addFirstActionType(name);
  let SORT = sortActionType(name);
  let DELETE = deleteActionType(name);
  let DELETE_LAST = deleteLastActionType(name);
  let CLEAR = clearActionType(name);

  return (state = initialValue, a) => {

    switch(a.type) {
      case MERGE:
        return mergeUnique(state, a.payload);
      case ADD:
        return [...state, a.payload];
      case SET:
        return [...a.id];
      case UPDATE:
        return [...a.id]
      case ADD_FIRST:
        return [a.payload, ...state];
      case SORT:
        return [...state].sort(a.payload);
      case DELETE:
        if(typeof a.payload === "function")
          return state.filter(i => !a.payload(i));

        return state.filter(i => i !== a.payload);
      case DELETE_LAST:
        return state.slice(0, -1);
      case CLEAR:
        return [];
      default:
        return state;
    }
  };
}

export const valueReducer = (name, initialValue = null) => {
  let SET = setActionType(name);
  let UPDATE = updateActionType(name);
  let DELETE = deleteActionType(name);
  let CLEAR = clearActionType(name);

  return (state = initialValue, a) => {
    switch(a.type) {
      case SET:
      case UPDATE:
        return a.id;
      case DELETE:
      case CLEAR:
        return null;
      default:
        return state;
    }
  };
}

export const setAction = name => (id, payload) => dispatch => dispatch({type: setActionType(name), id, payload});
export const updateAction = name => (id, payload) => dispatch => dispatch({type: updateActionType(name), id, payload});

export const mergeAction = name => (payload) => dispatch => dispatch({type: mergeActionType(name), payload});
export const addAction = name => (payload) => dispatch => dispatch({type: addActionType(name), payload});
export const addFirstAction = name => (payload) => dispatch => dispatch({type: addFirstActionType(name), payload});
export const sortAction = name => (payload) => dispatch => dispatch({type: sortActionType(name), payload});

export const deleteAction = name => (payload) => dispatch => dispatch({type: deleteActionType(name), payload});
export const deleteLastAction = name => (payload) => dispatch => dispatch({type: deleteLastActionType(name), payload});
export const clearAction = name => _ => dispatch => dispatch({type: clearActionType(name)});

export const namedActions = name => ({
  [`${name}Set`]: setAction(name),
  [`${name}Update`]: updateAction(name),

  [`${name}Merge`]: mergeAction(name),
  [`${name}Add`]: addAction(name),
  [`${name}AddFirst`]: addFirstAction(name),
  [`${name}Sort`]: sortAction(name),

  [`${name}Clear`]: clearAction(name),
  [`${name}Delete`]: deleteAction(name)
});

export const actions = name => ({
  set: setAction(name),
  update: updateAction(name),

  merge: mergeAction(name),
  add: addAction(name),
  addFirst: addFirstAction(name),
  sort: sortAction(name),

  clear: clearAction(name),
  delete: deleteAction(name),
  deleteLast: deleteLastAction(name)
});
