import Ajv from 'ajv';
import schema from './optionsSchema';

export default options => {
  // duplicate before validation, since it mutates by adding default values in schema to the data
  let data = { ...options };

  const ajv = new Ajv({ useDefaults: true });
  const validate = ajv.compile(schema);
  const valid = validate(data);

  return valid ? data : {};
}
